package Ships.application.Battleships.controllers;

import Ships.application.Battleships.model.User;
import Ships.application.Battleships.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.PostConstruct;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.*;
import java.util.UUID;

@Controller
@Validated
public class RegisterController {

    @Autowired
    private UserRepository userRepository;

    @PostConstruct
    public void init() {
        userRepository.save(new User(UUID.randomUUID().toString(), "admin", "admin", "admin", "admin", "admin", 0, 0, 0, 0, 0));
        userRepository.save(new User(UUID.randomUUID().toString(), "admin2", "admin2", "admin2", "admin2", "admin2", 0, 0, 0, 0, 0));
    }

    @PostMapping("/register")
    public String addUser(@Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String login, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String password,
                          @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String firstName, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String lastName,
                          @Validated @Email @RequestParam String email, @Validated @NotNull @Min(1919) @RequestParam Integer birthYear, Model model) {

        if (userRepository.existsByLogin(login)) {
            model.addAttribute("message", "user already exists, try register again!");
            model.addAttribute("messageType", "danger");
        } else {
            userRepository.save(new User(UUID.randomUUID().toString(), login, password, firstName, lastName, email, birthYear, 0, 0, 0, 0));
            model.addAttribute("message", "you are registered, please login!");
            model.addAttribute("messageType", "success");
        }
        return "home";
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody()
    public ModelAndView conflict(Model model) {

        model.addAttribute("message", "incorrect data, try register again!");
        model.addAttribute("messageType", "danger");

        return new ModelAndView("home");
    }
}
