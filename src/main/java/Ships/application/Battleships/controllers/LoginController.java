package Ships.application.Battleships.controllers;

import Ships.application.Battleships.model.User;
import Ships.application.Battleships.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Controller
@Validated
public class LoginController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/login")
    public ModelAndView login (@Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String login, @Validated @NotBlank @Size(min = 3, max = 30) @RequestParam String password,
                         HttpServletRequest req, Model model) {

        User user = userRepository.findByLogin(login);
        if (user != null && password.equals(user.getPassword())){
            req.getSession().setAttribute("login", user.getLogin());
            model.addAttribute("login", user.getLogin());
            return new ModelAndView("menu");
        }else{
            model.addAttribute("message", "invalid data");
            model.addAttribute("messageType", "danger");
            return new ModelAndView("home");
        }
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseBody
    public ModelAndView conflict(Model model) {
        model.addAttribute("message", "invalid data");
        model.addAttribute("messageType", "danger");
        return new ModelAndView("home");
    }
}
