package Ships.application.Battleships.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class User {

    @Id
    private String id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private String email;
    private int birthYear;
    private int won;
    private int lost;
    private int hit;
    private int misses;

    public void setWon() {
        this.won++;
    }

    public void setLost() {
        this.lost++;
    }

    public void setHit() {
        this.hit++;
    }

    public void setMisses() {
        this.misses++;
    }
}
