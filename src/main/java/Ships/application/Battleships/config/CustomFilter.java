package Ships.application.Battleships.config;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/*")
public class CustomFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;

        String session = (String) request.getSession().getAttribute("login");
        String requestURI = request.getRequestURI();

        if (
                session != null ||
                        requestURI.endsWith(".css") ||
                        requestURI.endsWith("home") ||
                        requestURI.endsWith("login") ||
                        requestURI.endsWith("register") ||
                        requestURI.endsWith("h2-console")
        ) {
            filterChain.doFilter(request, response);
        } else {
            response.sendRedirect(response.encodeRedirectURL("home"));
        }
    }

    @Override
    public void destroy() {

    }
}
