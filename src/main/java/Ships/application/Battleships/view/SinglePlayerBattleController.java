package Ships.application.Battleships.view;

import Ships.application.Battleships.model.User;
import Ships.application.Battleships.repository.UserRepository;
import Ships.application.Battleships.service.BattleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class SinglePlayerBattleController {

    @Autowired
    private BattleService battleService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/singlePlayerBattle")
    public ModelAndView singlePlayerBattle(Model model, HttpServletRequest req) {
        basicView(model, req);
        return new ModelAndView("singlePlayerBattle");
    }

    @GetMapping("/clear")
    public void clear(HttpServletResponse resp) throws IOException {
        battleService.clearArray();
        resp.sendRedirect(resp.encodeRedirectURL("singlePlayerBattle"));
    }

    @GetMapping("/setShips")
    public void setShips(HttpServletResponse resp) throws IOException {
        battleService.clearArray();
        battleService.setShips(battleService.getMap1());
        battleService.setShips(battleService.getMap2());
        resp.sendRedirect(resp.encodeRedirectURL("singlePlayerBattle"));
    }

    @PostMapping("/singlePlayerBattle")
    public ModelAndView shot(@RequestParam int x, @RequestParam int y, Model model, HttpServletRequest req) {
        battleService.shotMap2(x, y, req);
        User user = userRepository.findByLogin((String) req.getSession().getAttribute("login"));

        if (battleService.getPlayer2Life() == 0) {
            model.addAttribute("win", user.getLogin() + " win !");
            user.setWon();
            userRepository.save(user);
        } else {
            battleService.shotMap1SinglePlayer();
        }

        if (battleService.getPlayer1Life() == 0) {
            model.addAttribute("win", "BOT win !");
            user.setLost();
            userRepository.save(user);
        }

        basicView(model, req);
        return new ModelAndView("singlePlayerBattle");
    }

    private void basicView(Model model, HttpServletRequest req) {
        model.addAttribute("map1", battleService.getMap1());
        model.addAttribute("map2", battleService.getMap2());
        model.addAttribute("player1Life", battleService.getPlayer1Life());
        model.addAttribute("player2Life", battleService.getPlayer2Life());
        model.addAttribute("player1", req.getSession().getAttribute("login"));
    }
}
