package Ships.application.Battleships.view;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class MenuController {

    @GetMapping("/menu")
    public ModelAndView menu(HttpServletRequest req, Model model) {
        String login = (String) req.getSession().getAttribute("login");

        model.addAttribute("login", login);
        return new ModelAndView("menu");
    }
}
