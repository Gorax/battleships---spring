package Ships.application.Battleships.view;

import Ships.application.Battleships.model.User;
import Ships.application.Battleships.repository.UserRepository;
import Ships.application.Battleships.service.BattleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class MultiOnTwoPc {

    @Autowired
    private BattleService battleService;

    @Autowired
    private UserRepository userRepository;

    private boolean flag = false;

    @GetMapping("/multiPlayer")
    public ModelAndView multiView(HttpServletRequest req, Model model) {
        battleService.setPlayers(req);

        if (req.getSession().getAttribute("login").equals(battleService.getLoginPlayer1())) {
            basicView(model);
            return new ModelAndView("player1Multi");
        } else if (req.getSession().getAttribute("login").equals(battleService.getLoginPlayer2())) {
            basicView(model);
            return new ModelAndView("player2Multi");
        } else {
            model.addAttribute("login", req.getSession().getAttribute("login"));
            model.addAttribute("busy", "The game room is currently occupied");
            return new ModelAndView("menu");
        }
    }

    @PostMapping("/multiPlayer1Shot")
    public ModelAndView multiPlayer1Shot(@RequestParam int x, @RequestParam int y, Model model, HttpServletRequest req) throws InterruptedException {
        battleService.shotMap2(x, y, req);
        checkWin(model);
        basicView(model);
        flag = false;

        while (true) {
            Thread.sleep(2000);
            if (flag)
                break;
        }
        return new ModelAndView("player1Multi");
    }

    @PostMapping("/multiPlayer2Shot")
    public ModelAndView multiPlayer2Shot(@RequestParam int x, @RequestParam int y, Model model, HttpServletRequest req) throws InterruptedException {
        battleService.shotMap1(x, y, req);
        checkWin(model);
        basicView(model);
        flag = true;

        while (true) {
            Thread.sleep(2000);
            if (!flag)
                break;
        }
        return new ModelAndView("player2Multi");
    }

    @GetMapping("/multiSetShipsPlayer1")
    public void setShipsPlayer1(HttpServletResponse resp) throws IOException {
        battleService.clearArrayPlayer1();
        battleService.setShips(battleService.getMap1());
        resp.sendRedirect(resp.encodeRedirectURL("multiPlayer"));
    }

    @GetMapping("/multiSetShipsPlayer2")
    public void setShipsPlayer2(HttpServletResponse resp) throws IOException {
        battleService.clearArrayPlayer2();
        battleService.setShips(battleService.getMap2());
        resp.sendRedirect(resp.encodeRedirectURL("multiPlayer"));
    }

    @GetMapping("backToMenu")
    public void backToMenu(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String login = (String) req.getSession().getAttribute("login");
        flag = false;

        if (battleService.getLoginPlayer1() != null && login.equals(battleService.getLoginPlayer1())) {
            battleService.setLoginPlayer1(null);
            battleService.clearArrayPlayer1();
            resp.sendRedirect(resp.encodeRedirectURL("menu"));
        } else if (battleService.getLoginPlayer2() != null && login.equals(battleService.getLoginPlayer2())) {
            battleService.setLoginPlayer2(null);
            battleService.clearArrayPlayer2();
            resp.sendRedirect(resp.encodeRedirectURL("menu"));
        }
    }

    private void basicView(Model model) {
        model.addAttribute("map1", battleService.getMap1());
        model.addAttribute("map2", battleService.getMap2());
        model.addAttribute("player1", battleService.getLoginPlayer1());
        model.addAttribute("player2", battleService.getLoginPlayer2());
        model.addAttribute("player1Life", battleService.getPlayer1Life());
        model.addAttribute("player2Life", battleService.getPlayer2Life());
    }

    private void checkWin(Model model) {
        if (battleService.getPlayer2Life() == 0) {
            model.addAttribute("win", battleService.getLoginPlayer1() + " win !");
            User user1 = userRepository.findByLogin(battleService.getLoginPlayer1());
            User user2 = userRepository.findByLogin(battleService.getLoginPlayer2());
            user1.setWon();
            user2.setLost();
            userRepository.save(user1);
            userRepository.save(user2);
        } else if (battleService.getPlayer1Life() == 0) {
            model.addAttribute("win", battleService.getLoginPlayer2() + " win !");
            User user2 = userRepository.findByLogin(battleService.getLoginPlayer2());
            User user1 = userRepository.findByLogin(battleService.getLoginPlayer1());
            user2.setWon();
            user1.setLost();
            userRepository.save(user1);
            userRepository.save(user2);
        }
    }
}
