package Ships.application.Battleships.view;

import Ships.application.Battleships.model.User;
import Ships.application.Battleships.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;

@Controller
public class StatisticsController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/statistics")
    public ModelAndView statistics (HttpServletRequest req, Model model){
        String login = (String) req.getSession().getAttribute("login");
        User user = userRepository.findByLogin(login);

        model.addAttribute("login", login);
        model.addAttribute("won", user.getWon());
        model.addAttribute("lost", user.getLost());
        model.addAttribute("hit", user.getHit());
        model.addAttribute("misses", user.getMisses());
        return new ModelAndView("statistics");
    }
}
