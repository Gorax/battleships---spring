package Ships.application.Battleships.view;

import Ships.application.Battleships.model.User;
import Ships.application.Battleships.repository.UserRepository;
import Ships.application.Battleships.service.BattleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class MultiOnOnePc {

    @Autowired
    private BattleService battleService;

    @Autowired
    private UserRepository userRepository;

    @GetMapping("/player1")
    public ModelAndView player1(Model model, HttpServletRequest req) {
        basicMultiView(model, req);
        return new ModelAndView("player1");
    }

    @GetMapping("/player2")
    public ModelAndView player2(Model model, HttpServletRequest req) {
        basicMultiView(model, req);
        return new ModelAndView("player2");
    }

    @GetMapping("/clearMulti")
    public void clearPlayer1(HttpServletResponse resp) throws IOException {
        battleService.clearArray();
        resp.sendRedirect(resp.encodeRedirectURL("player1"));
    }

    @GetMapping("/setShipsPlayer1")
    public void setShipsPlayer1(HttpServletResponse resp) throws IOException {
        battleService.clearArrayPlayer1();
        battleService.setShips(battleService.getMap1());
        resp.sendRedirect(resp.encodeRedirectURL("player1"));
    }

    @GetMapping("/setShipsPlayer2")
    public void setShipsPlayer2(HttpServletResponse resp) throws IOException {
        battleService.clearArrayPlayer2();
        battleService.setShips(battleService.getMap2());
        resp.sendRedirect(resp.encodeRedirectURL("player2"));
    }

    @PostMapping("/player1")
    public ModelAndView shotPlayer1(@RequestParam int x, @RequestParam int y, Model model, HttpServletRequest req) {
        battleService.shotMap2(x, y, req);
        checkWin(model, req);

        basicMultiView(model, req);
        return new ModelAndView("player2");
    }

    @PostMapping("/player2")
    public ModelAndView shotPlayer2(@RequestParam int x, @RequestParam int y, Model model, HttpServletRequest req) {
        battleService.shotMap1(x, y, req);
        checkWin(model, req);

        basicMultiView(model, req);
        return new ModelAndView("player1");
    }

    private void basicMultiView(Model model, HttpServletRequest req) {
        model.addAttribute("map1", battleService.getMap1());
        model.addAttribute("map2", battleService.getMap2());
        model.addAttribute("player1Life", battleService.getPlayer1Life());
        model.addAttribute("player2Life", battleService.getPlayer2Life());
        model.addAttribute("player1", req.getSession().getAttribute("login"));
    }

    private void checkWin(Model model, HttpServletRequest req) {
        User user = userRepository.findByLogin((String) req.getSession().getAttribute("login"));
        if (battleService.getPlayer2Life() == 0) {
            model.addAttribute("win", user.getLogin() + " win !");
            user.setWon();
            userRepository.save(user);
        } else if (battleService.getPlayer1Life() == 0) {
            model.addAttribute("win", "Player 2 win !");
            user.setLost();
            userRepository.save(user);
        }
    }
}
