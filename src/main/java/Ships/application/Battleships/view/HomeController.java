package Ships.application.Battleships.view;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @GetMapping({"/","","/home"})
    public ModelAndView homeView(){
        ModelAndView modelAndView = new ModelAndView("home");
        return modelAndView;
    }
}
