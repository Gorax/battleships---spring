package Ships.application.Battleships.service;

import Ships.application.Battleships.model.User;
import Ships.application.Battleships.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Random;

@Service
public class BattleService {

    @Autowired
    private UserRepository userRepository;

    private Integer[][] map1 = new Integer[10][10];
    private Integer[][] map2 = new Integer[10][10];
    private int player1Life;
    private int player2Life;
    private String loginPlayer1 = null;
    private String loginPlayer2 = null;

    private Random random = new Random();


    @PostConstruct
    public void init() {
        clearArray();
    }

    public Integer[][] getMap1() {
        return map1;
    }

    public Integer[][] getMap2() {
        return map2;
    }

    public int getPlayer1Life() {
        return player1Life;
    }

    public int getPlayer2Life() {
        return player2Life;
    }

    public void shotMap1SinglePlayer() {
        while (true) {
            int x = random.nextInt(10);
            int y = random.nextInt(10);

            if (map1[x][y] == null) {
                map1[x][y] = 1;
                break;
            } else if (map1[x][y] == 3) {
                map1[x][y] = 2;
                player1Life--;
                break;
            }
        }
    }

    public void shotMap1(int x, int y, HttpServletRequest req) {
        User user = userRepository.findByLogin((String) req.getSession().getAttribute("login"));
        if (map1[y][x] == null) {
            map1[y][x] = 1;
            user.setMisses();
        } else if (map1[y][x] == 3) {
            map1[y][x] = 2;
            player1Life--;
            user.setHit();
        }
        userRepository.save(user);
    }

    public void shotMap2(int x, int y, HttpServletRequest req) {
        User user = userRepository.findByLogin((String) req.getSession().getAttribute("login"));
        if (map2[y][x] == null) {
            map2[y][x] = 1;
            user.setMisses();
        } else if (map2[y][x] == 3) {
            map2[y][x] = 2;
            player2Life--;
            user.setHit();
        }
        userRepository.save(user);
    }

    public void setShips(Integer[][] map) {
        for (int i = 5; i > 0; i--) {
            int shipSize = i;
            boolean flag = true;

            while (flag) {
                boolean orientation = random.nextBoolean();
                int positionY = random.nextInt(9);
                int positionX = random.nextInt(9);

                if (orientation == true) {

                    if (positionY >= shipSize &&
                            check(positionY, positionX, shipSize, map, true)
                    ) {

                        for (int j = 0; j < shipSize; j++) {

                            map[positionY - j][positionX] = 3;
                        }
                        flag = false;
                    }

                } else {

                    if (positionX >= shipSize &&
                            check(positionY, positionX, shipSize, map, false)
                    ) {

                        for (int j = 0; j < shipSize; j++) {

                            map[positionY][positionX - j] = 3;
                        }
                        flag = false;
                    }
                }
            }
        }
    }

    private boolean check(int y, int x, int shipSize, Integer map[][], boolean orientation) {
        if (x == 0 && orientation && y != 9 && y != 0) {
            for (int i = 0; i < shipSize + 2; i++) {
                if (map[(y + 1) - i][x] != null || map[(y + 1) - i][x + 1] != null)
                    return false;
            }
            return true;
        }

        if (x == 0 && orientation && y == 9) {
            for (int i = 0; i < shipSize + 1; i++) {
                if (map[y - i][x] != null || map[y - i][x + 1] != null)
                    return false;
            }
            return true;
        }

        if (x == 9 && orientation && y != 9 && y != 0) {
            for (int i = 0; i < shipSize + 2; i++) {
                if (map[(y + 1) - i][x] != null || map[(y + 1) - i][x - 1] != null) {
                    return false;
                }
            }
            return true;
        }

        if (x == 9 && orientation && y == 9) {
            for (int i = 0; i < shipSize + 1; i++) {
                if (map[y - i][x] != null || map[y - i][x - 1] != null)
                    return false;
            }
            return true;
        }


        if (y == 0 && !orientation && x != 9 && x != 0) {
            for (int i = 0; i < shipSize + 2; i++) {
                if (map[y][(x + 1) - i] != null || map[y + 1][(x + 1) - i] != null)
                    return false;
            }
            return true;
        }

        if (y == 0 && !orientation && x == 9) {
            for (int i = 0; i < shipSize + 1; i++) {
                if (map[y][x - i] != null || map[y + 1][x - i] != null)
                    return false;
            }
            return true;
        }

        if (y == 9 && !orientation && x != 9 && x != 0) {
            for (int i = 0; i < shipSize + 2; i++) {
                if (map[y][(x + 1) - i] != null || map[y - 1][(x + 1) - i] != null) {
                    return false;
                }
            }
            return true;
        }

        if (y == 9 && !orientation && x == 9) {
            for (int i = 0; i < shipSize + 1; i++) {
                if (map[y][x - i] != null || map[y - 1][x - i] != null)
                    return false;
            }
            return true;
        }


        if (orientation) {
            for (int i = 0; i < shipSize + 2; i++) {
                if (map[(y + 1) - i][x - 1] != null || map[(y + 1) - i][x] != null || map[(y + 1) - i][x + 1] != null)
                    return false;
            }
            return true;
        }

        if (orientation) {
            for (int i = 0; i < shipSize + 2; i++) {
                if (map[y - 1][(x + 1) - i] != null || map[y][(x + 1) - i] != null || map[y + 1][(x + 1) - i] != null)
                    return false;
                return true;
            }
        }
        return false;
    }

    public void clearArray() {
        player1Life = 15;
        player2Life = 15;

        for (int i = 0; i < map1.length; i++) {
            for (int j = 0; j < map1.length; j++) {
                map1[i][j] = null;
                map2[i][j] = null;
            }
        }
    }

    public void clearArrayPlayer1() {
        player1Life = 15;

        for (int i = 0; i < map1.length; i++) {
            for (int j = 0; j < map1.length; j++) {
                map1[i][j] = null;
            }
        }
    }

    public void clearArrayPlayer2() {
        player2Life = 15;

        for (int i = 0; i < map1.length; i++) {
            for (int j = 0; j < map1.length; j++) {
                map2[i][j] = null;
            }
        }
    }

    public String getLoginPlayer1() {
        return loginPlayer1;
    }

    public String getLoginPlayer2() {
        return loginPlayer2;
    }

    public void setLoginPlayer1(String loginPlayer1) {
        this.loginPlayer1 = loginPlayer1;
    }

    public void setLoginPlayer2(String loginPlayer2) {
        this.loginPlayer2 = loginPlayer2;
    }

    public void setPlayers(HttpServletRequest req) {
        String login = (String) req.getSession().getAttribute("login");

        if (loginPlayer1 == null && !login.equals(loginPlayer2)) {
            loginPlayer1 = login;
        } else if (loginPlayer2 == null && !login.equals(loginPlayer1)) {
            loginPlayer2 = login;
        }
    }
}
