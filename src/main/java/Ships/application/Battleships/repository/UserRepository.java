package Ships.application.Battleships.repository;

import Ships.application.Battleships.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, String> {

    List<User> findAll();

    boolean existsByLogin(String login);

    User findByLogin(String login);
}
